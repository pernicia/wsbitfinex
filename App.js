import { StatusBar } from "expo-status-bar";
import React from "react";
import { StyleSheet, Text, View } from "react-native";
import { NavigationContainer } from "@react-navigation/native";
import { createStackNavigator } from "@react-navigation/stack";
import { Home } from "./src/components/home";
import { OrderBook, Ticker, Trades } from "./src/components/screens";
const StackApp = createStackNavigator();
import { Provider } from "react-redux";
import store from "./src/store";
export default function App() {
  return (
    <Provider store={store}>
      <NavigationContainer>
        <StackApp.Navigator initialRouteName="Home">
          <StackApp.Screen name="Home" component={Home}></StackApp.Screen>
          <StackApp.Screen
            name="OrderBook"
            component={OrderBook}
          ></StackApp.Screen>
          <StackApp.Screen name="Ticker" component={Ticker}></StackApp.Screen>
          <StackApp.Screen name="Trades" component={Trades}></StackApp.Screen>
        </StackApp.Navigator>
      </NavigationContainer>
    </Provider>

    // <View style={styles.container}>
    //   <Text>Bitfinex Mobile Programming Challenge!</Text>
    //   <Home/>
    //   <StatusBar style="auto" />
    // </View>
  );
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: "#fff",
    alignItems: "center",
    justifyContent: "center",
  },
});
