const initialState = {
  data: [],
};

const TradesReducer = (state = initialState, action) => {
  switch (action.type) {
    case "myApiTradesData": {
      console.log("Value from UI", action.payload);
      console.log("initialState", initialState);
      var { data } = state;
      data.push(action.payload);
      const newState = { data };
      return newState;
    }
    case "myApiTradesDataClose": {
      return state;
    }
    default:
      return state;
  }
};

export default TradesReducer;
