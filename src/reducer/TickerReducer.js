const initialState = {
  data: [],
};

const TickerReducer = (state = initialState, action) => {
  switch (action.type) {
    case "myHandleClick": {
      console.log("Value from UI", action.payload);
      console.log("initialState", initialState);
      var { data } = state;
      data.push(action.payload);
      const newState = { data };
      return newState;
    }
    case "myApiData": {
      console.log("Value from UI", action.payload);
      console.log("initialState", initialState);
      var { data } = state;
      data = [];
      data.push(action.payload);
      const newState = { data };
      return newState;
    }
    case "myApiDataClose": {
      return state;
    }
    default:
      return state;
  }
};

export default TickerReducer;
