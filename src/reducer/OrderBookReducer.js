const initialState = {
  data: [],
};

const OrderBookReducer = (state = initialState, action) => {
  switch (action.type) {
    case "myApiOrderBookData": {
      var { data } = state;
      data.push(action.payload);
      const newState = { data };
      return newState;
    }
    case "myApiOrderBookDataClose": {
      return state;
    }
    default:
      return state;
  }
};

export default OrderBookReducer;
