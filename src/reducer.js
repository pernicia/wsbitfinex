import {combineReducers} from 'redux'
import TickerReducer from './reducer/TickerReducer';
import OrderBookReducer from './reducer/OrderBookReducer';
import TradesReducer from './reducer/TradesReducer';

const rootReducer = combineReducers({
    TickerReducer:TickerReducer,
    OrderBookReducer:OrderBookReducer,
    TradesReducer:TradesReducer
})

export default rootReducer