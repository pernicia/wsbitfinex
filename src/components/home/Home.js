import React from 'react';
import { View,Text, TouchableOpacity } from 'react-native';
import {COLORS,FONTS} from '../../constants'
const Home = ({navigation})=>{
    const handleOrderBook = ()=>{
        console.log("handleOrderBook");
        navigation.navigate('OrderBook');
    }
    const handleTrades = ()=>{
        console.log("handleTrades");
        navigation.navigate('Trades');
    }
    const handleTicker = ()=>{
        console.log("handleTicker");
        navigation.navigate('Ticker');
    }
    return(
        <View>
            <View style={{backgroundColor:COLORS.BLACK,height:50,justifyContent:'center',alignItems:'center'}}>
                <Text style={{color:COLORS.WHITE}}>Bitfinex Mobile Programming Challenge!</Text>
            </View>
            
            <TouchableOpacity
                onPress={handleOrderBook}
                style={{backgroundColor:COLORS.secondary,height:100,padding:5,
                    justifyContent:"center",alignItems:'center',margin:5}}
            >
                <View style={{justifyContent:"center",alignItems:'center'}}>
                    <Text style={{color:COLORS.WHITE,...FONTS.h2}}>OrderBook</Text>
                </View>
            </TouchableOpacity>

            <TouchableOpacity
                onPress={handleTicker}
                style={{backgroundColor:COLORS.secondary,height:100,padding:5,
                    justifyContent:"center",alignItems:'center',margin:5}}
            >
                 <View style={{justifyContent:"center",alignItems:'center'}}>
                    <Text style={{color:COLORS.WHITE,...FONTS.h2}}>Ticker</Text>
                </View>
            </TouchableOpacity>
            <TouchableOpacity
                onPress={handleTrades}
                style={{backgroundColor:COLORS.secondary,height:100,padding:5,
                    justifyContent:"center",alignItems:'center',margin:5}}
            >
                 <View style={{justifyContent:"center",alignItems:'center'}}>
                    <Text style={{color:COLORS.WHITE,...FONTS.h2}}>Trades</Text>
                </View>
            </TouchableOpacity>
        </View>
    )
}

export default Home