import React from "react";
import { View, Text, Button } from "react-native";
import { COLORS } from "../../constants";
import { useSelector, useDispatch } from "react-redux";
const OrderBook = () => {
  const data = useSelector((state) => {
    // console.log("State", state.TradesReducer);
    return state.OrderBookReducer;
  });
  const dispatch = useDispatch();
  const handleConnect = () => {
    var ws = new WebSocket("wss://api-pub.bitfinex.com/ws/2");
    let msg = JSON.stringify({
      event: "subscribe",
      channel: "book",
      symbol: "tBTCUSD",
    });
    ws.onopen = () => {
      ws.send(msg);
    };
    ws.onmessage = (event) => {
      dispatch({ type: "myApiOrderBookData", payload: event.data });
    };
  };
  const handleDisconnect = () => {
    //console.log("handle disconnect");
    var ws = new WebSocket("wss://api-pub.bitfinex.com/ws/2");
    ws.close();
    dispatch({ type: "myApiOrderBookDataClose", payload: [] });
  };
  return (
    <View>
      <View
        style={{
          flexDirection: "row",
          justifyContent: "space-between",
          backgroundColor: COLORS.BLACK,
        }}
      >
        <View style={{ flexDirection: "row" }}>
          <View style={{ justifyContent: "center", marginLeft: 10 }}>
            <Text style={{ color: COLORS.WHITE }}>TRADING BOOK</Text>
          </View>
        </View>
        <View style={{ flexDirection: "row" }}>
          <View style={{ marginLeft: 10, justifyContent: "center" }}>
            <Text style={{ color: COLORS.WHITE, padding: 10 }}>-</Text>
          </View>
          <View style={{ marginLeft: 10, justifyContent: "center" }}>
            <Text style={{ color: COLORS.WHITE, padding: 10 }}>+</Text>
          </View>
        </View>
      </View>
      <View
        style={{
          backgroundColor: "black",
        }}
      >
        <View style={{ flexDirection: "row" }}>
          <View style={{ flex: 1 }}>
            <View style={{ justifyContent: "center", marginLeft: 10 }}>
              <Text style={{ color: COLORS.WHITE }}>TOTAL</Text>
            </View>
          </View>
          <View style={{ flex: 1 }}>
            <View style={{ justifyContent: "center", marginLeft: 10 }}>
              <Text style={{ color: COLORS.WHITE }}>PRICE</Text>
            </View>
          </View>
          <View style={{ flex: 1 }}>
            <View style={{ justifyContent: "center", marginLeft: 10 }}>
              <Text style={{ color: COLORS.WHITE }}>PRICE</Text>
            </View>
          </View>
          <View style={{ flex: 1 }}>
            <View style={{ justifyContent: "center", marginLeft: 10 }}>
              <Text style={{ color: COLORS.WHITE }}>TOTAL</Text>
            </View>
          </View>
          {/* <View style={{flex:1}}>
                        <View style={{justifyContent:'center',marginLeft:10}}>
                            <Text style={{color:'white'}}>AMOUNT</Text>
                        </View>
                    </View> */}
        </View>
      </View>
      <View
        style={{
          flexDirection: "row",
          justifyContent: "center",
          alignItems: "center",
        }}
      >
        <View
          style={{
            justifyContent: "center",
            alignItems: "center",
            margin: 10,
          }}
        >
          <Button onPress={handleConnect} title="Connect"></Button>
        </View>
        <View
          style={{
            justifyContent: "center",
            alignItems: "center",
            margin: 10,
          }}
        >
          <Button onPress={handleDisconnect} title="Disconnect"></Button>
        </View>
      </View>
      <View>
        {data.data.map((val, index) => {
          return (
            <View key={index}>
              <Text>{val}</Text>
            </View>
          );
        })}
      </View>
    </View>
  );
};

export default OrderBook;
