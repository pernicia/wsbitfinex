import React, { useEffect, useState } from "react";
import { View, Text, Button } from "react-native";
import { COLORS } from "../../constants";
import { useSelector, useDispatch } from "react-redux";

const Trades = () => {
  const data = useSelector((state) => {
    // console.log("State", state.TradesReducer);
    return state.TradesReducer;
  });
  const dispatch = useDispatch();
  const handleConnect = () => {
    var ws = new WebSocket("wss://api-pub.bitfinex.com/ws/2");
    let msg = JSON.stringify({
      event: "subscribe",
      channel: "trades",
      symbol: "tBTCUSD",
    });
    ws.onopen = () => {
      ws.send(msg);
    };
    ws.onmessage = (event) => {
      dispatch({ type: "myApiTradesData", payload: event.data });
    };
  };
  const handleDisconnect = () => {
    //console.log("handle disconnect");
    var ws = new WebSocket("wss://api-pub.bitfinex.com/ws/2");
    ws.close();
    dispatch({ type: "myApiTradesDataClose", payload: [] });
  };

  return (
    <View>
      <View>
        <View
          style={{
            flexDirection: "row",
            justifyContent: "space-between",
            backgroundColor: COLORS.BLACK,
          }}
        >
          <View style={{ flexDirection: "row" }}>
            <View style={{ justifyContent: "center", marginLeft: 10 }}>
              <Text style={{ color: COLORS.WHITE }}>TRADES</Text>
            </View>
          </View>
          <View style={{ flexDirection: "row" }}>
            <View style={{ marginLeft: 10, justifyContent: "center" }}>
              <Text style={{ color: COLORS.WHITE, padding: 10 }}>Market</Text>
            </View>
          </View>
        </View>
        <View
          style={{
            backgroundColor: "black",
          }}
        >
          <View style={{ flexDirection: "row" }}>
            <View style={{ flex: 1 }}>
              <View style={{ justifyContent: "center", marginLeft: 10 }}>
                <Text style={{ color: COLORS.WHITE }}>TIME</Text>
              </View>
            </View>
            <View style={{ flex: 3 }}>
              <View style={{ justifyContent: "center", marginLeft: 10 }}>
                <Text style={{ color: COLORS.WHITE }}>PRICE</Text>
              </View>
            </View>
            <View style={{ flex: 1 }}>
              <View style={{ justifyContent: "center", marginLeft: 10 }}>
                <Text style={{ color: COLORS.WHITE }}>AMOUNT</Text>
              </View>
            </View>
          </View>
        </View>
        <View
          style={{
            flexDirection: "row",
            justifyContent: "center",
            alignItems: "center",
          }}
        >
          <View
            style={{
              justifyContent: "center",
              alignItems: "center",
              margin: 10,
            }}
          >
            <Button onPress={handleConnect} title="Connect"></Button>
          </View>
          <View
            style={{
              justifyContent: "center",
              alignItems: "center",
              margin: 10,
            }}
          >
            <Button onPress={handleDisconnect} title="Disconnect"></Button>
          </View>
        </View>
        <View>
          {data.data.map((val, index) => {
            return (
              <View key={index}>
                <Text>{val}</Text>
              </View>
            );
          })}
        </View>
      </View>
    </View>
  );
};

export default Trades;
