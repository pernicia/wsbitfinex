import React, { useEffect } from "react";
import { View, Text, Button } from "react-native";
import { COLORS } from "../../constants";
import { useSelector, useDispatch } from "react-redux";

const Ticker = () => {
  const data = useSelector((state) => {
    console.log("State", state.TickerReducer);
    return state.TickerReducer;
  });
  const dispatch = useDispatch();
  const handleConnect = () => {
    var ws = new WebSocket("wss://api-pub.bitfinex.com/ws/2");
    let msg = JSON.stringify({
      event: "subscribe",
      channel: "ticker",
      symbol: "tBTCUSD",
    });
    ws.onopen = () => {
      ws.send(msg);
    };
    ws.onmessage = (event) => {
      //console.log("Data", event.data);
      dispatch({ type: "myApiData", payload: event.data });
      //   setShowData(true);
      //   setApiData(event.data);
    };
    // console.log("Handle Connect in Ticker");
    // dispatch({ type: "myHandleClick", payload: "My Data from ticker" });
    // var ws = new WebSocket("wss://api-pub.bitfinex.com/ws/2");
    // let msg = JSON.stringify({
    //   event: "subscribe",
    //   channel: "ticker",
    //   symbol: "tBTCUSD",
    // });
    // ws.onopen = () => {
    //   ws.send(msg);
    // };
    // ws.onmessage = (event) => {
    //   console.log("Data", event.data);
    //   //   setShowData(true);
    //   //   setApiData(event.data);
    // };
  };
  const handleDisconnect = () => {
    console.log("handle disconnect");
    var ws = new WebSocket("wss://api-pub.bitfinex.com/ws/2");
    ws.close();
    dispatch({ type: "myApiDataClose", payload: [] });
  };
  //   useEffect(() => {
  //     var ws = new WebSocket("wss://api-pub.bitfinex.com/ws/2");
  //     let msg = JSON.stringify({
  //       event: "subscribe",
  //       channel: "ticker",
  //       symbol: "tBTCUSD",
  //     });
  //     ws.onopen = () => {
  //       ws.send(msg);
  //     };
  //     ws.onmessage = (event) => {
  //       //console.log("Data", event.data);
  //       dispatch({ type: "myApiData", payload: event.data });
  //       //   setShowData(true);
  //       //   setApiData(event.data);
  //     };
  //   });
  return (
    <View>
      <View
        style={{
          flexDirection: "row",
          justifyContent: "space-between",
          backgroundColor: COLORS.BLACK,
          height: 100,
        }}
      >
        <View style={{ flexDirection: "row" }}>
          <View style={{ justifyContent: "center", marginLeft: 10 }}>
            <Text style={{ color: COLORS.WHITE }}>Bitcoin</Text>
          </View>
          <View style={{ marginLeft: 10, justifyContent: "center" }}>
            <Text style={{ color: COLORS.WHITE }}>BTC/USD</Text>
            <Text style={{ color: COLORS.WHITE }}>VOL</Text>
            <Text style={{ color: COLORS.WHITE }}>LOW</Text>
          </View>
        </View>
        <View style={{ flexDirection: "row" }}>
          <View style={{ marginLeft: 10, justifyContent: "center" }}>
            <Text style={{ color: COLORS.WHITE }}>Num</Text>
            <Text style={{ color: COLORS.WHITE }}>Up</Text>
            <Text style={{ color: COLORS.WHITE }}>High</Text>
          </View>
        </View>
      </View>
      <View
        style={{
          flexDirection: "row",
          justifyContent: "center",
          alignItems: "center",
        }}
      >
        <View
          style={{ justifyContent: "center", alignItems: "center", margin: 10 }}
        >
          <Button onPress={handleConnect} title="Connect"></Button>
        </View>
        <View
          style={{ justifyContent: "center", alignItems: "center", margin: 10 }}
        >
          <Button onPress={handleDisconnect} title="Disconnect"></Button>
        </View>
      </View>

      <View>
        {data.data.map((val, index) => {
          return (
            <View key={index}>
              <Text>{val}</Text>
            </View>
          );
        })}
      </View>
    </View>
  );
};

export default Ticker;
